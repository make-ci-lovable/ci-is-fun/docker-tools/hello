FROM node:15-slim

RUN npm install -g node-fetch

# some tools
COPY hello.js /usr/local/bin/hello
RUN chmod +x /usr/local/bin/hello

# this allows to require global modules (like node-fetch)
#ENV NODE_PATH=/usr/lib/node_modules
ENV NODE_PATH=/usr/local/lib/node_modules

CMD ["/bin/sh"]
